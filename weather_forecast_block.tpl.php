<?php
/**
 * @file
 * Template file for weather forecast block
 *
 * Available variables:
 *   $forecast['content'] - weather forecast data to be shown in block
 *   $forecast['links']   - links "view forecast for 5 days" and if form in block enabled   "open options" link
 *   $forecast['options'] - form where user can select their city
 *
 * @see template_preprocess_weather_forecast_block().
 */
?>
<div class = 'weather-forecast-current'>
  <?php echo render($forecast['content']) ?> 

  <?php echo render($forecast['links']) ?>
</div>

<div id="weather-forecast-block-options">
  <?php echo render($forecast['options']) ?>
</div>

