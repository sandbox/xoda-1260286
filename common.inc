<?php

/**
 * @file
 *
 */

/**
 * function which reads XML elements and puts it into array
 */
function weather_forecast_read_xml_elements($xml, $split_xml = FALSE) {
  $element = array();
  preg_match_all('/<(\w+)\s*([^\/>]*)\s*(?:\/>|>(.*?)<(\/\s*\1\s*)>)/s', $xml, $array_params);
  foreach ($array_params[1] as $param_key => $tag_name) {
    if (strpos($array_params[3][$param_key], '<') !== FAlSE) {
      if ($split_xml == TRUE) {
        $subelements = weather_forecast_read_xml_elements($array_params[3][$param_key], $split_xml);
        foreach ($subelements as $key => $value) {
          $element[$tag_name . '_' . $key] = $value;
        }
      }
      continue;
    }
    $element[$tag_name] = $array_params[3][$param_key];
  }

  return $element;
}

/**
 * Helper function which get cloud and return image name
 *
 * @param int $cloud
 *   value of cloud
 * @param int $hour
 *   hour
 *
 * @return string
 *    image name
 */
function weather_forecast_cloud_image($cloud = 0, $hour = 0) {
  $num = floor($cloud/10);
  $day_part =  weather_forecast_hours_to_day_part($hour);
  $image = '';

  if ($day_part == 3 && $num == 0) {
    $image = '_0_moon.gif';
  }
  elseif ($num == 0) {
    $image = '_0_sun.gif';
  }

  if ($day_part == 3 && $num == 1) {
    $image = '_1_moon_cl.gif';
  }
  elseif ($num == 1) {
    $image = '_1_sun_cl.gif';
  }

  switch ($num) {
    case 2:
      $image = '_2_cloudy.gif';
      break;

    case 3:
      $image = '_3_pasmurno.gif';
      break;

    case 4:
      $image = '_4_short_rain.gif';
      break;

    case 5:
      $image = '_5_rain.gif';
      break;

    case 6:
      $image = '_6_lightning.gif';
      break;

    case 7:
      $image = '_7_hail.gif';
      break;

    case 8:
      $image = '_8_rain_swon.gif';
      break;

    case 9:
      $image = '_9_snow.gif';
      break;

    case 10:
      $image = '_10_heavy_snow.gif';
      break;
  }

  return $image;
}

/**
 * Helper function to convert int value of cloud into readable name
 *
 * @param int $cloud
 *    value of clouds
 *
 * @return string
 *    readable name representing level of clouds
 */
function weather_forecast_convert_clouds($clouds = '') {
  $clouds_string = '';

  if ($clouds != '') {
    $clouds = floor($clouds/10);

    switch ($clouds) {
      case '0':
        $clouds_string = t('Clear');
        break;

      case '1':
        $clouds_string = t('Clear, partly cloudy');
        break;

      case '2':
        $clouds_string = t('Cloudy');
        break;

      case '3':
        $clouds_string = t('Mostly cloudy');
        break;

      case '4':
        $clouds_string = t('Short rain');
        break;

      case '5':
        $clouds_string = t('Rain');
        break;

      case '6':
        $clouds_string = t('Rain and lighting');
        break;

      case '7':
        $clouds_string = t('Hail');
        break;

      case '8':
        $clouds_string = t('Sleet');
        break;

      case '9':
        $clouds_string = t('Snow');
        break;

      case '10':
        $clouds_string = t('Heavy show');
        break;
    }
  }

  return $clouds_string;
}

/**
 * Helper function to convert wind direction from degrees to readable name
 *
 * @param int $wind
 *    wind direction in degrees(0-360)
 *
 * @return string
 *    word representing current wind direction
 */
function weather_forecast_convert_wind_direction($wind = '') {
  $wind_string = '';

  if ($wind >= 0 && $wind <= 360) {
    if ($wind >= 0 && $wind < 20) {
      $wind_string = t('N');
    }
    elseif ($wind > 20 && $wind <= 35) {
      $wind_string = t('N-NE');
    }
    elseif ($wind > 35 && $wind <= 55) {
      $wind_string = t('NE');
    }
    elseif ($wind > 55 && $wind <= 70) {
      $wind_string = t('E-NE');
    }
    elseif ($wind > 70 && $wind <= 110) {
      $wind_string = t('E');
    }
    elseif ($wind > 110 && $wind <= 125) {
      $wind_string = t('E-SE');
    }
    elseif ($wind > 125 && $wind <= 145) {
      $wind_string = t('SE');
    }
    elseif ($wind > 145 && $wind <= 160) {
      $wind_string = t('S-SE');
    }
    elseif ($wind > 160 && $wind <= 200) {
      $wind_string = t('S');
    }
    elseif ($wind > 200 && $wind <= 215) {
      $wind_string = t('S-SW');
    }
    elseif ($wind > 215 && $wind <= 235) {
      $wind_string = t('SW');
    }
    elseif ($wind > 235 && $wind <= 250) {
      $wind_string = t('W-SW');
    }
    elseif ($wind > 250 && $wind <= 290) {
      $wind_string = t('W');
    }
    elseif ($wind > 290 && $wind <= 305) {
      $wind_string = t('W-NW');
    }
    elseif ($wind > 305 && $wind <= 325) {
      $wind_string = t('NW');
    }
    elseif ($wind > 325 && $wind <= 340) {
      $wind_string = t('N-NW');
    }
    else {
      $wind_string = t('N');
    }
  }

  return $wind_string;
}

/**
 * Helper function to convert hours into string represinting current part of the day
 *
 * @param int $hour
 *    Current hour
 * @param boolean $toString
 *    defines convert hour to readable name or not
 *
 * @return string,int $day_part
 *    Readable current part of the day or hour representing current part of the day(3,9,15 or 21)
 */
function weather_forecast_hours_to_day_part($hour = '', $to_string = FALSE) {
  $day_part = '';

  if ($hour >= 0 && $hour <= 23) {

    if ($hour > 3 && $hour <= 9) {
      $day_part = 9;
    }
    elseif ($hour > 9  && $hour <= 15) {
      $day_part = 15;
    }
    elseif ($hour > 15 && $hour <= 21) {
      $day_part = 21;
    }
    else {
      $day_part = 3;
    }

    if ($to_string) {
      switch ($day_part) {
        case '3':
          $day_part = t('At night');
          break;
        case '9':
          $day_part = t('In the morning');
          break;
        case '15':
          $day_part = t('In the afternoon');
          break;
        case '21':
          $day_part = t('In the evening');
          break;
      }
    }
  }

  return $day_part;
}

/**
 * This function connects to weather.co.ua and take list of the countries.
 */
function weather_forecast_import_countries() {
  $added_count = 0;
  if (($fp = fopen('http://xml.weather.co.ua/1.2/country/', "r")) !== FALSE) {
    $file_content = fread($fp, 4096);

    # checking version
    if (preg_match('/country version="1.2"/', $file_content)) {

      while (!feof($fp)) {
        $file_content .= fread($fp, 16384);

        # get all country
        if (preg_match_all('/<country id="(\d+)">(.*?)<\/country>/is', $file_content, $found_element)) {

          foreach ($found_element[2] as $link_country_id => $inner_element_data) {
            $country = weather_forecast_read_xml_elements($inner_element_data, TRUE);
            $country['id'] = $found_element[1][$link_country_id];
            $sql = "INSERT INTO `weather_country` (`country_id`, `country_name_ru`, `country_name_en`) VALUES (:id, :name, :name_en)
                    ON DUPLICATE KEY UPDATE `country_name_ru` = :name, `country_name_en` = :name_en";

            $args = array(
                  ':id'        => $country['id'],
                  ':name'      => $country['name'],
                  ':name_en'   => $country['name_en'],
            );

            if (db_query($sql, $args)) {
                $added_count++;
                $file_content = str_replace($found_element[0][$link_country_id], '', $file_content);
            }
          }
        }
      }
      drupal_set_message(t('Added @count countries.', array('@count' => $added_count)), 'status', FALSE);
    }
    else {
      $text  = t('Version of the forecast should be XML 1.2. You should update the module.');
      drupal_set_message($text, 'error', TRUE);
    }
  }
  else {
    $text  = t('Unable to connect to retrieve the weather forecast.');
    drupal_set_message($text, 'error', TRUE);
    watchdog('IMweather_forecast', 'Unable to connect to retrieve the weather forecast.', 'WATCHDOG_ERROR');
  }
}

/**
 * This function connects to weather.co.ua and take list of the cities.
 */
function weather_forecast_import_cities() {
  $added_count = 0;

  # get city data
  if (($fp = fopen('http://xml.weather.co.ua/1.2/city/', 'r')) !== FALSE) {
    $file_content = fread($fp, 4096);

    # checking version
    if (preg_match('/city version="1.2"/', $file_content)) {

      while (!feof($fp)) {
        $file_content .= fread($fp, 16384);

        # get all city
        if (preg_match_all('/<city id="(\d+)">(.*?)<\/city>/is', $file_content, $found_element)) {

          foreach ($found_element[2] as $link_city_id => $inner_element_data) {
            $city = weather_forecast_read_xml_elements($inner_element_data, TRUE);

            $city['id'] = $found_element[1][$link_city_id];

            $sql = "INSERT INTO {weather_city} (city_id, city_name_ru, country_id, city_name_en) VALUES (:id, :name, :c_id, :name_en)
                      ON DUPLICATE KEY UPDATE city_name_ru = :name, country_id = :c_id, city_name_en = :name_en";

            $args = array(
              ':id'        => $city['id'],
              ':c_id'      => $city['country_id'],
              ':name'      => !empty($city['name']) ? $city['name'] : $city['name_en'],
              ':name_en'  => $city['name_en']
            );

            if (db_query($sql, $args)) {
              $added_count++;
              $file_content = str_replace($found_element[0][$link_city_id], '', $file_content);
            }
          }
        }
      }
      drupal_set_message(t('Added @count cities.', array('@count' => $added_count)), 'status', FALSE);
    }
    else {
      $text  = t('Version of the forecast should be XML 1.2. You should update the module.');
      drupal_set_message($text, 'error', TRUE);
    }
  }
  else {
    $text  = t('Unable to connect to retrieve the weather forecast.');
    drupal_set_message($text, 'error', TRUE);
    watchdog('IMweather_forecast', 'Unable to connect to retrieve the weather forecast.', 'WATCHDOG_ERROR');
  }
}

/**
 * Download weather forecast data
 */
function weather_forecast_import_forecast() {
  $added_count = 0;

  db_query("TRUNCATE TABLE weather_forecast");
  $days = variable_get('weather_forecast_days', 5);

    if (($fp = fopen("http://xml.weather.co.ua/1.2/fullforecast/" . "?dayf=" . $days , "r")) !== FALSE) {
        $file_content = stream_get_contents($fp);
        # checking forecast version
        if (preg_match('/version="1.2"/', $file_content)) {

          # get all city forecast
          if (preg_match_all('/<forecast city="(\d+)">(.*?)<\/forecast>/is', $file_content, $forecast_found)) {
            foreach ($forecast_found[2] as $link_city_id => $inner_forecast_found) {
            $city_id = $forecast_found[1][$link_city_id];
                # get city forecast divided by date-hour
                preg_match_all('@<day date="(\d{4}-\d{2}-\d{2})" hour="(\d{1,2})">(.*?)</day>@is', $inner_forecast_found, $hour_data);

                foreach ($hour_data[3] as $key => $params) {
                  $forecast = weather_forecast_read_xml_elements($params, TRUE);
                  $forecast['pict'] = weather_forecast_cloud_image($forecast['cloud'], $hour_data[2][$key]);

                  $sql = "INSERT INTO {weather_forecast} (`city_id`, `date`, `hour`, `cloud`, `pict`, `precip`, `t_min`,
                            `t_max`, `p_min`, `p_max`, `w_min`, `w_max`, `w_rumb`, `h_min`, `h_max`, `wpi`)
                          VALUES (:city_id, :date, :hour, :cloud, :pict, :precip, :t_min, :t_max, :p_min, :p_max, :wind_min, :wind_max,
                          :wind_rumb, :h_min, :h_max, :wpi)
                          ON DUPLICATE KEY UPDATE `cloud` = :cloud, `pict` = :pict, `precip` = :precip, `t_min` = :t_min,
                            `t_max` = :t_max, `p_min` = :p_min, `p_max` = :p_max, `w_min` = :wind_min,
                            `w_max` = :wind_max, `w_rumb` = :wind_rumb, `h_min` = :h_min, `h_max` = :h_max,
                            `wpi` = :wpi";
                  $args = array(
                      ':city_id'   => $city_id,
                      ':date'      => $hour_data[1][$key],
                      ':hour'      => $hour_data[2][$key],
                      ':cloud'     => $forecast['cloud'],
                      ':pict'      => $forecast['pict'],
                      ':precip'    => $forecast['ppcp'],
                      ':t_min'     => $forecast['t_min'],
                      ':t_max'     => $forecast['t_max'],
                      ':p_min'     => $forecast['p_min'],
                      ':p_max'     => $forecast['p_max'],
                      ':wind_min'  => $forecast['wind_min'],
                      ':wind_max'  => $forecast['wind_max'],
                      ':wind_rumb' => $forecast['wind_rumb'],
                      ':h_min'     => $forecast['h_min'],
                      ':h_max'     => $forecast['h_max'],
                      ':wpi'       => $forecast['wpi'],
                    );


                  if (db_query($sql, $args)) {
                    $added_count++;
                  }
              }
            }
          }
        }
        else {
          $text  = t('Version of the forecast should be XML 1.2. You should update the module.');
          drupal_set_message($text, 'error', TRUE);
          return;
        }
      }
      else {
        $text  = t('Unable to connect to retrieve the weather forecast.');
        drupal_set_message($text, 'error', TRUE);
        watchdog('IMweather_forecast', 'Unable to connect to retrieve the weather forecast.', 'WATCHDOG_ERROR');
        return;
      }

    drupal_set_message(t('Added @count entries of weather forecast', array('@count' => $added_count)), 'status', FALSE);
}
