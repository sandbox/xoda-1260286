(function($) {
   window.onload = function() {
     open_options = $('#open-options');
     close_options = $('#close-options');
       
     left = $('.block-weather-forecast .content .weather-forecast-current').outerWidth(true);
       
     open_options.click(function() {
       $('.block-weather-forecast .content').animate({
         left: -left
       }, 1000); 
     });
      
     close_options.click(function() {  
       $('.block-weather-forecast .content').animate({
         left: 0
       }, 1000); 
     });
   };
})(jQuery);