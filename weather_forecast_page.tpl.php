<?php
/**
 * @file
 * Template file for weather forecast page for the selected city
 *
 * Available variables:
 *   $forecast['link']    - link to select another city
 *   $forecast['content'] - weather forecast data by days
 *
 * @see template_preprocess_weather_forecast_page().
 */
?>
<div class="weather-forecast-page">
  <div class="select-city">
    <span class="select-city-description"><?php echo t('View weather forecast in other city') ?>:</span>
    <?php echo $forecast['link'] ?>
  </div>
  
  <?php echo render($forecast['content']) ?>
</div>
