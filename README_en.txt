/**
 * Installation
 */
1. Copy the module into the following folder - /sites/all/modules
2. Enable it at modules page - admin/build/modules
3. Place weather forecast block in any region you want


/**
 * Configuration
 */
You can configure module at page - admin/config/services/weather-forecast
  

/**
 * Developers
 */
Module created by Khodanyonok Andrey
LLC "Internet-Marketing" http://www.internet-marketing.by


/**
 * Notes
 */
Module updates weather forecast at every cron launch. So its needn't to worry about weather forecast updates. If cron enabled it will be always up to date.

Finding bugs and any ideas about module is appreciated. If you want to make some changes - please send a message to xoda91@gmail.com.
