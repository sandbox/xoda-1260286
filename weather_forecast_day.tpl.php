<?php
/**
 * @file
 * Template file for weather forecast day table
 *
 * Available variables:
 *   $forecast['date']  - timestamp of the day
 *   $forecast['table'] - table with weather forecast data for the day
 *
 * @see template_preprocess_weather_forecast_day().
 */
?>
<div class="forecast-day">
  <div class="forecast-date">
    <?php echo format_date($forecast['date'], 'custom', 'l, j F') ?>
  </div>

  <?php echo render($forecast['table']) ?>
</div>
